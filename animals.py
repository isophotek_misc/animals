# -*- coding: utf-8 -*-

import os
from sys import maxint
from random import choice, randint
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import exc
from sqlalchemy import desc
import networkx as nx
# import matplotlib.pyplot as plt

Base = declarative_base()

# setup for the database session
engine = create_engine('sqlite:///animals.db')
dbSession = sessionmaker()
dbSession.configure(bind=engine)
session = dbSession()

Base.metadata.bind = engine


class Entity(Base):
    """
    represents a single entity record in the entity table
    hunger is None if race is NOT Lion
    speed is None if race is NOT Antelope
    """
    __tablename__ = 'entity'
    id = Column(Integer, primary_key=True)
    name = Column(String(25), nullable=False)
    race = Column(Integer)
    hunger = Column(Integer)  # only applies to Lion entities, all other set this to None
    speed = Column(Integer)  # only applies to Antelope entities, all other set this to None

    def __repr__(self):
        return self.name


class Relationship(Base):
    """
    represents a relationship between two entities
    """
    __tablename__ = 'relations'
    id = Column(Integer, primary_key=True)
    entityId = Column(Integer, ForeignKey('entity.id'))
    vectorTo = Column(Integer, ForeignKey('entity.id'))


def dbCreate():
    """
    creates the database

    :return: nothing
    """
    Base.metadata.create_all(engine)


def dbCreateEntities():
    """
    populates the database entity table with 10 entities of each race defined in raceList

    :return: nothing
    """
    raceList = ['Lion', 'Antelope', 'Hippo', 'Hyena']  # list of possible species to create

    # create ten of each species in the database
    for currentRace in raceList:
        for index in range(10):
            newEntity = createEntity(currentRace)
            session.add(newEntity)
    session.commit()


def dbCreateRelations():
    """
    generates relationships between entities in the entities table based on entity id,
    relationships are stored in the relations table.

    :return: nothing
    """
    # list of entities in the database - ID, race format (pulled from entity table)
    # also used as a list of possible candidates for relations
    entityList = session.query(Entity.id, Entity.race)

    for entity in entityList:  # loop through each entity in the database
        existingRelationsList = []  # list if entities the current entity has already connections with
        numOfFriends = randint(0, 10)  # number of relations to establish for current entity
        for friendship in range(numOfFriends):  # create number of connections equal to 'numOfFriends'
            # create the relationship object
            newFriendship = createRelationship(entity, entityList, existingRelationsList)
            existingRelationsList = newFriendship[1]  # update the list of already established relations
            session.add(newFriendship[0])  # add the Relation object to the database
    session.commit()


def createRelationship(parent, entityList, existingRelationsList):
    """
    creates a relationship for the parent entity from the entityList.
    Already existing relationships are added to the existingRelationsList and passed back with the resulting
    Relationship object.

    :param parent: Entity object
        the entity seeking relationships
    :param entityList: list
        list of possible entities to create relationships with
    :param existingRelationsList: list
        list of entities the parent already has relationships with

    :return: tuple pair: Relationship object, list of existing relationships of the current entity
    """
    newParent = parent.id  # get the entity id of the parent object

    chosenFriend = choice(entityList.all())  # choose a random entity from the list to create a relationship with
    newVectorTo = chosenFriend.id  # get the id of the chosen entity
    while newVectorTo in existingRelationsList:  # choose again if the relationship already exists
        newVectorTo = choice(entityList.all()).id
    existingRelationsList.append(newVectorTo)  # add the new relationship to the list of existing relations

    # create the Relationship object
    newRelationship = Relationship(entityId=newParent, vectorTo=newVectorTo)
    return newRelationship, existingRelationsList


def createEntity(race):
    """
    creates a single Entity object for the entities table

    :param race: string
        specifies the race of the entity. this is important for the evaluation of attributes hunger and speed,
        as not all races have these.

    :return: Entity object
    """

    newEntityName = getRandomName()  # get a name for the entity
    newEntityRace = race  # set race of the entity

    # set hunger value for lions
    if race is 'Lion':
        newEntityHunger = randint(0, 100)
    else:
        newEntityHunger = None

    # set speed value for antelopes
    if race is 'Antelope':
        newEntitySpeed = randint(0, 100)
    else:
        newEntitySpeed = None

    # create the Entity object
    newEntity = Entity(name=newEntityName, race=newEntityRace, hunger=newEntityHunger, speed=newEntitySpeed)
    return newEntity


def dbCheck():
    """
    checks if the database exists and its content is valid.
    :return: nothing
    """
    if not os.path.exists('./animals.db'):  # check if the database file exists. create database if not.
        print 'no database found, creating one from scratch'
        dbCreate()
        dbCreateEntities()
        dbCreateRelations()
    try:  # check if Entity table exists and has exactly 40 records
        numberOfEntities = session.query(Entity)
        if numberOfEntities.count() != 40:
            for record in session.query(Entity).all():
                session.delete(record)
            dbCreateEntities()
    except (exc.SQLAlchemyError, exc.OperationalError) as e:  # create the Entity table if necessary
        print e.message
        dbCreate()
        dbCreateEntities()
    try:  # check if Relations table can be queried
        session.query(Relationship)
    except (exc.SQLAlchemyError, exc.OperationalError) as e:  # create the Relations table if necessary
        print e.message
        dbCreate()
        dbCreateRelations()


def getRandomName():
    """
    generates a random name from two predefined list of syllables, just for fun...

    :return: string
    """
    syllableDict = {'first': ['Ab', 'Ac', 'Ad', 'Bre', 'Bar', 'Cyl', 'Cre', 'Dru', 'En', 'Er', 'Fam'],
                    'second': ['rak', 'car', 'kuk', 'vak', 'vuk', 'dur', 'dhen', 'lam', 'dor', 'hir', 'fur']}
    newName = choice(syllableDict['first']) + choice(syllableDict['second'])
    return newName


def getEntityById(entityId):
    """
    returns the Entity object from the database matching entityId

    :param entityId: integer

    :return: Entity object
    """
    return session.query(Entity).filter(Entity.id == entityId).first()


def getHunter():
    """
    returns the Entity object from the database with the highest hunger value

    :return: Entity object
    """
    return session.query(Entity).filter(Entity.race == 'Lion').order_by(desc(Entity.hunger)).limit(1).first()


def getPrey():
    """
    returns the Entity object from the database with the lowest speed value

    :return: Entity object
    """
    return session.query(Entity).filter(Entity.race == 'Antelope').order_by(Entity.speed).limit(1).first()


def getPathToPrey():
    """
    finds the shortest path in the relation network.

    creates a graph for the relationship network using the networkx package. relations are weighted according to the
    entities race it is pointing to. Lion and Hippo entities get a lower value, as they are the preferred route for
    the traversal along the nodes.

    The shortest route is calculated using the Dijkstra's algorithm, also contained within the networkx package.

    :return: list: list of Entity objects along the shortest path in the relation network
                returns None if no path exists
    """
    relationshipGraph = nx.DiGraph()  # create the directional graph instance

    # assign weight to edges
    # weight value is if relation points to a Lion or Hippo
    # weight value is maximum integer value if relation points to a Antelope or Hyena
    relations = session.query(Relationship.entityId, Relationship.vectorTo)
    for relation in relations:
        if getEntityById(relation.entityId).race == 'Hyena' or getEntityById(relation.entityId).race == 'Antelope':
            weight = maxint
        else:
            weight = 1
        # add edges to the graph
        relationshipGraph.add_edge(getEntityById(relation.entityId), getEntityById(relation.vectorTo), weight=weight)

    # find shortest path from hunter to prey using dijkstra algorithm
    try:
        dijkstraPath = nx.dijkstra_path(relationshipGraph, getHunter(), getPrey())
    except nx.NetworkXNoPath as e:
        print e.message
        return None

    # graphColormap = ['b']
    # for item in range(1, len(dijkstraPath) - 1):
    #     graphColormap.append('g')
    # graphColormap.append('r')
    # nx.draw_circular(relationshipGraph,
    #                  node_size=2500,
    #                  with_labels=True,
    #                  nodelist=dijkstraPath,
    #                  node_color=graphColormap)
    # plt.show()

    return dijkstraPath


if __name__ == '__main__':
    if os.path.exists('./animals.db'):
        prompt = None
        while prompt is None:
            prompt = raw_input('create new database? [y/n] ')
            prompt = prompt.lower()
            if prompt == 'y':
                os.remove('animals.db')  # delete the database file, dbCheck() will create a new one
            elif prompt == 'n':
                pass
            else:
                print 'invalid input'
                prompt = None
    dbCheck()  # perform database check
    shortestPath = getPathToPrey()  # get the shortest path from hunter to prey
    if shortestPath is None:
        print 'no path to the prey exists. the hunter will starve.'
    else:
        print 'Hunter: %s' % getHunter().name
        print 'Prey: %s' % getPrey().name
        print '-------------------------'
        print 'number of steps: %s' % len(shortestPath)
        print 'path:'
        for entity in shortestPath:
            print '%s, %s, %s' % (entity.id, entity.name, entity.race)
        session.close()
