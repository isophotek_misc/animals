# Feladat

A szavannán 4 állatfaj található: oroszlán, víziló, antilop és hiéna, minden állatfajból 10-10 példány
él ott. Minden példány rendelkezik névvel és életkorral, valamint mindenkinek vannak a 4
állatfajból ismerősei, melyek száma 0-10 között változik. Az oroszlánokra jellemző egy éhség, az
antilopokra pedig egy sebesség érték amely a többi állatfajnál nem található meg.
A legéhesebb oroszlán szeretne ismerősein keresztül eljutni a leglassabb antilophoz, hogy legyen
esélye elkapni. Ha egy antilopot útba ejt, ő lehet, hogy riasztja a többieket. Ha egy hiénát ejt útba, ő
is kérni fog a lakomából. Tehát úgy kell eljutnia a keresett antilophoz, hogy közben a legkevesebb
antilopot és hiénát érintse.
A megoldás során ki kell alakítani a megfelelő relációs adatmodellt, majd létre kell hozni a
példányokat, a közöttük lévő kapcsolatok véletlenszerű kialakításával.

# Problémák

A feladat megoldásához két fő kérdést kell tisztázni. Az első a modell felállítása és hogy ezt hogyan tároljuk adatbázisban oly módon, hogy az lehetőleg kiszolgálja a matematikai megoldást.

A második probléma maga a matematikai megközelítése a feladatnak. A kapcsolatrendszeren belüli navigálás tulajdonképpen egy útvonal probléma, amit egy gráfban lehet ábrázolni, ahhol az egyes vertexek maguk az egyedek, a közöttük lévő élek pedig az "ismeretségek". Az éleket súlyozzuk "járhatóságuk" szerint, így találhatjuk meg az optimális útvonalat.

## Adatbázis

Az adatbázisban két fogalmat tárolunk. Egy tábla tartalmazza az egyedeket és azok tulajdonságait. Ebben a táblában minden lehetséges tulajdnság szerepel mint oszlop, akkor is ha bizonyos tulajdonságok csak bizonyos fajokhoz tartozhatnak (lásd éhség és sebesség). Ezt adatbázisban úgy ábrázoljuk, hogy ha egy faj nem rendelkezik egy adott tulajdonsággal, akkor ennek értéke a táblában NULL. így egységesen tárolhatunk minden egyedet, illetve a programban se ábrázoljuk az egyes fajokat külön osztályként egyetlen attribútum miatt. A feladat megoldásához nem járulna hozzá az egyes fajok külön osztállyal való ábrázoláása. Minden fajt ugyanaz az Entity osztály képvisel, az éhség és sebesség tulajdonságok jelentőségét az adatbászisból való lekérdezéskor kezeljük, hiszen tulajdonképpen csak egy sorrendiség felállítására használjuk amikor kiválasztjuk a legéhesebb oroszlán, illetve leglassabb antilop egyeet.

  Egy másik tábla az egyedek közötti kapcsolatokat tartalmazza, pontosabban az egyedek saját táblája szerinti egyedi azonoítójukat. Ezek egy irányú vektorok, tehát feltételezzük, hogy ha A ismeri B-t, az nem jelenti azt, hogy B ismeri A-t. Minden ilyen kapcsolat egy rekord. Ebből fakadóan a kapcsolatok gráfját ebből a táblából könnyedén felépíthetjük.
  
## Útvonal

A feladat matematikai problémája egy útvonal kérdés. Jelen esetben egy irányított gráfról beszélünk, mivel ahogy már fentebb írtam a kapcsolatoknak van iránya. Emellett az alkalmazandó gráf súlyozott is, így reprezentálhatjuk, hogy azon kapcsolatok, melyek oroszlán vagy viziló egyedre mutatnak elsőbbséget élveznek a hiénára vagy antilopra mutatóakkal szemben. A gráf felállításakor ezt, figyelembe vesszük és ennek megfelelően minden él hordoz egy értéket, mely vagy 1 (oroszlán vagy viziló esetén) vagy az integer maximális értéke (hiéna és antilop). A két érték között így megfelelően nagy az eltérés, hogy a nem preferált éleket csak a legszükségesebb esetekben járjuk be.

Pythonban a networkx csomag ideális erre, mive nem csak a digráf van már benne definiálva, de a Dijkstra algoritmus is, emellett használata is igen egyszerű, ezért erre a problémára ezt alkalmaztam.

![](./sample_generated_graph.png "Kapcsolat rendszer")

![](./dijkstra_graph.png "Megoldás Dijkstra algoritmussal")

# A program működése

## dependenciák

- sqlalchemy
- sqlite3
- networkx

## Lefutás

- Adatbázis fájl létének ellenőrzése, annak létrehozása ha az nem található, beleértve a táblákat és azok tartalmát.
- Amennyiben az adatbázis lézetik eldönthetjük, hogy azt használjuk, vagy újat hozunk létre. Új adatbázis esetében a régit fájl szinten töröljük, és az újat a feladatnak megfelelően benépesítjük. (Egyedek és kapcsolatok létrehozása)
- Adatbázis tartalmának ellenőrzése; táblák létezése, egyedek létszámának ellenőrzése.
- A gráf objektum létrehozása
- Útvonal megkeresése a "vadász" és "pérda" között.
- Eredmény kiírása:
    - Név szerint ki a vadász, ki a préda
    - Hány lépésből áll az útvonal
    - A vadász, az útvonalon lévő egyedek és a préda, lista fomában (azonosítő, név, faj)